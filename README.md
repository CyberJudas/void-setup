# void-setup

Void Linux setup script. Installs custom build of `dwm+st` and its dependencies, along with some other utilities.
Just run the `void-deployer.sh` file after making it executable with `sudo chmod +x ./void-deployer.sh`